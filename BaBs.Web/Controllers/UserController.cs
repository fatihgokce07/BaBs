using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaBs.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using BaBs.Web.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using Newtonsoft.Json;
namespace BaBs.Web.Controllers {
    [Authorize("Bearer")]
    [Route ("api/[controller]")]
    public class UserController : BaseController {
        BaBsContext session;
        public UserController (BaBsContext context) {
                session = context;
        }
        [HttpGet]
        public JsnResponse Get (int? page, string currentUser) {
             int baslangic = ((page??1) - 1) * PAGE_COUNT;
             //new string[] { "value1", "value2"};
             User user=JsonConvert.DeserializeObject<User>(currentUser);
             var l = this.session.Users;
             JsnResponse jsn=new JsnResponse();
             //Where(x=>x.IsActive==true)
             var q=l.Where(x=>
             (string.IsNullOrEmpty(user.NameSurname) ? true:x.NameSurname.Contains(user.NameSurname))
             && (string.IsNullOrEmpty(user.Email) ? true:x.Email.Contains(user.Email))
             && (string.IsNullOrEmpty(user.MobilPhone) ? true :x.MobilPhone.Contains(user.MobilPhone))
             );
             jsn.ToplamSayi=q.Count(); 
             jsn.Data=q.OrderByDescending(x=>x.CreatedDate).Skip(baslangic).Take(PAGE_COUNT);
             return jsn;
        }       
        [HttpGet("getUserLog")]
        public JsnResponse GetUserLog(int? page){
            JsnResponse res=new JsnResponse();
            int baslangic = ((page??1) - 1) * PAGE_COUNT;
            var q= session.UserLogs.Include(x=>x.User);
            res.ToplamSayi=q.Count();
            res.Data=q.OrderByDescending(x => x.LoginDate).Skip(baslangic).Take(PAGE_COUNT);
            return res;

        }
        //[HttpGet("API/{apiname}", Name = "GetLAPI")]
        [HttpGet("ExistUser")]
        public bool ExistUser(string tableName,string columnName,string value,int? id)
        {
            if(id.HasValue)
            {
                int count=session.Users.FromSql($"SELECT * FROM {tableName} WHERE {columnName} = @p0 and Id!={id.Value}", value).Count();
                return count>0;
            }else{
                int count=session.Users.FromSql($"SELECT * FROM {tableName} WHERE {columnName} = @p0", value).Count();
                return count>0;
            }
           
        }
        // GET api/<controller>/5
        [HttpGet("{id:int}")]
        public User Get(int id)
        {
            return session.Users.FirstOrDefault(x=>x.Id==id);
        }
        // POST api/<controller>
        [HttpPost]
        public void Post ([FromBody] User value) {
            if(value.Id>0)
            {

                session.Entry(value).State = EntityState.Modified;
                value.UpdatedDate=DateTime.Now;
                session.SaveChanges();
            }else{
                value.CreatedDate=DateTime.Now;
                session.Users.Add(value);
                session.SaveChanges();
            }
        }
         // PUT api/values/5
        [HttpPut ("{id}")]
        public void Put (int id, [FromBody] User value) { 
            session.Entry(value).State = EntityState.Modified;
            value.UpdatedDate=DateTime.Now;
            session.SaveChanges();

        }
    }
}