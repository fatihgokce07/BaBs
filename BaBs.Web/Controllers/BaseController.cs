using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaBs.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BaBs.Web.Controllers
{
    public class BaseController:Controller
    {
        public const int PAGE_COUNT=2; 
    }
}