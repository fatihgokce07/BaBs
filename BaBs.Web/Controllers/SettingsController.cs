using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BaBs.Data;
using BaBs.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;
namespace BaBs.Web.Controllers {
    public class ModelView {
        public Setting setting { get; set; }
        public IFormFile file { get; set; }

    }
    [Authorize ("Bearer")]
    [Route ("api/[controller]")]
    public class SettingsController : BaseController {
        BaBsContext session;
        public bool IsDebug {
            get {
                bool isDebug = false;
#if DEBUG
                isDebug = true;
#endif
                return isDebug;
            }
        }
        public IConfiguration Configuration { get; }

        public SettingsController (BaBsContext context,IConfiguration configuration) {
            session = context;
            this.Configuration=configuration;
        }
        [HttpGet]
        public Setting Get () {
            return session.Settings.FirstOrDefault ();
        }
        [AllowAnonymous]
        [HttpPost ("upload")]
        //[Route("/api/upload")]
        public async Task Upload (IFormFile file) {
            if (file == null) throw new Exception ("File is null");
            if (file.Length == 0) throw new Exception ("File is empty");

            using (Stream stream = file.OpenReadStream ()) {
                using (var binaryReader = new BinaryReader (stream)) {
                    var fileContent = binaryReader.ReadBytes ((int) file.Length);
                    //await _uploadService.AddFile(fileContent, file.FileName, file.ContentType);
                }
            }
        }
        // POST api/values
        [HttpPost]
        public async Task Post (IFormFile file) {
            var data = Request.Form["setting"];

            Setting setting = JsonConvert.DeserializeObject<Setting> (data);
           

            if (file != null && file.Length > 0) {
                //wwwroot\\src\\assets\\upload debug
                //wwwroot\\assets\\upload
                string directory = "";
                bool isDevelopment=bool.Parse(Configuration["IsDeveleopment"]);
                if (isDevelopment) {
                    string pth1="src";
                    string pth2="assets";
                    string pth3="upload";
                    directory = Path.Combine (Directory.GetCurrentDirectory (), "wwwroot");
                    directory=Path.Combine(directory,pth1,pth2,pth3);
                }else{
                    //Path.Combine (Directory.GetCurrentDirectory (), "wwwroot\\assets\\upload");
                    string pth1="assets";
                    string pth2="upload";
                  
                    directory = Path.Combine (Directory.GetCurrentDirectory (), "wwwroot");
                    directory=Path.Combine(directory,pth1,pth2);
                }
                directory = Path.Combine (directory, file.FileName);
                using (var stream = new FileStream (directory, FileMode.Create)) {
                    await file.CopyToAsync (stream);
                }

                setting.LogoPath = "assets/upload/" + file.FileName;
            }

            if (setting != null && setting.Id > 0) {

                session.Entry (setting).State = EntityState.Modified;
                session.SaveChanges ();
            } else {
                session.Settings.Add (setting);
                session.SaveChanges ();
            }
        }

        // PUT api/values/5
        [HttpPut ("{id}")]
        public void Put (int id, [FromBody] string value) { }

        // DELETE api/values/5
        [HttpDelete ("{id}")]
        public void Delete (int id) { }

    }
}