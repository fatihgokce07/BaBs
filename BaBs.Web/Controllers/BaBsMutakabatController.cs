using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BaBs.Data;
using BaBs.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;
using BaBs.Web.ViewModels;
namespace BaBs.Web.Controllers
{
    [Authorize ("Bearer")]
    [Route ("api/[controller]")]
    public class BaBsMutakabatController:BaseController
    {
         BaBsContext session;
         public BaBsMutakabatController(BaBsContext context){
             this.session=context;
         }
        [HttpGet("getAll")]
        public JsnResponse GetAll (int? page,string model) 
        {     
            
            int baslangic = ((page??1) - 1) * PAGE_COUNT;
            BaBsMutakabatViewModel mutakabat=JsonConvert.DeserializeObject<BaBsMutakabatViewModel>(model);
            var l = this.session.BaBsMutakabat;
            JsnResponse jsn=new JsnResponse();
            //(isId.HasValue ? SqlFunctions.StringConvert((double)x.Id).Contains(isId.ToString()) : true) 
            var qq=(from m in session.BaBsMutakabat
                    join b in session.BaBsCaris on m.CariKod  equals b.CARI_KOD
                    where m.Yil==b.YIL && m.Ay==b.AY
                    select new {
                        CariKod=m.CariKod,CariIsim=b.CARI_ISIM,
                        Yil=m.Yil.ToString(),Ay=m.Ay.ToString(),
                        CreatedDate=m.CreatedDate,UpdatedDate=m.UpdatedDate,
                        Durum= m.Durum,Id=m.Id
                    }
                    );
           // string sql=IQueryableExtensions.ToSql(qq);
            var q=qq.Where(x=>
             (string.IsNullOrEmpty(mutakabat.CariKod) ? true:x.CariKod.Contains(mutakabat.CariKod))
             && (mutakabat.Yil==0 ? true: x.Yil.Contains(mutakabat.Yil.ToString()))      
             && (string.IsNullOrEmpty(mutakabat.CariIsim) ? true: x.CariIsim.Contains(mutakabat.CariIsim) )    
             );
            jsn.ToplamSayi=q.Count(); 
            jsn.Data=q.OrderByDescending(x=>x.CreatedDate).Skip(baslangic).Take(PAGE_COUNT);
            return jsn;
            
        }
        [HttpGet]
        public object Get (string cariKodu) 
        {
            int[]yillar=session.BaBsCaris.Where(x=>x.CARI_KOD==cariKodu).Select(x=>x.YIL).Distinct().ToArray();
            int[]aylar=session.BaBsCaris.Where(x=>x.CARI_KOD==cariKodu).Select(x=>x.AY).Distinct().ToArray();
            return new{Yillar=yillar,Aylar=aylar};
        }
        
        [HttpGet("GetCari")]
        public CCariBaBs GetCari(string cariKodu,int yil,int ay) 
        {
            //session.Set().FromSql("")
           
            
            CCariBaBs cari=new CCariBaBs();
            using(DbCommand command=session.Database.GetDbConnection().CreateCommand())
            {
                command.CommandText="GetCariBaBs";
                command.CommandType=CommandType.StoredProcedure;
                var pr1=new MySqlParameter();
                pr1.ParameterName="pcariKod";
                pr1.Value=cariKodu;
                var pr2=new MySqlParameter();
                pr2.ParameterName="pyil";
                pr2.Value=yil;
                var pr3=new MySqlParameter();
                pr3.ParameterName="pay";
                pr3.Value=ay;
                command.Parameters.Add(pr1);
                command.Parameters.Add(pr2);
                command.Parameters.Add(pr3);
                if(command.Connection.State==ConnectionState.Closed){
                    command.Connection.Open();
                }
                using(var reader=command.ExecuteReader()){
                    var list=reader.MapToList<CCariBaBs>();
                    if(list.Count>0)
                    {
                        cari=list[0];
                    }
                   //https://docs.microsoft.com/en-us/aspnet/core/data/ef-mvc/advanced

                }
                 if(command.Connection.State==ConnectionState.Open){
                    command.Connection.Close();
                }

            }
           
            
            return cari;
        }
         // GET api/values/5
        [HttpGet ("{id}")]
        public string Get (int id) {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post ([FromBody] BaBsMutakabat value) { 
           
            if(value.Id>0){
                var mutakabat=session.BaBsMutakabat.AsNoTracking().SingleOrDefault(x=>x.Id==value.Id);
                session.Entry(value).State = EntityState.Modified;
                value.UpdatedDate=DateTime.Now;    
                value.CreatedDate=mutakabat.CreatedDate;          

            } else{
                bool dahaOnceVarmi=session.BaBsMutakabat
                    .Where(x=>x.Ay==value.Ay && x.Yil== value.Yil && value.CariKod==value.CariKod)
                    .Count()>0;
                if(dahaOnceVarmi){
                    throw new Exception($"Daha önceden yil:{value.Yil} ay:{value.Ay}  için muatakabat yapılmış");
                }
                value.CreatedDate=DateTime.Now;
                session.BaBsMutakabat.Add(value);
            }
            var userAgent = Request.Headers["User-Agent"];
            UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);
            var ip= Request.HttpContext.Connection.RemoteIpAddress;
            MutakabatLog log=new MutakabatLog();
            log.Browser=$"{ua.Browser.Name} ${ua.Browser.Version}";
            log.Ip=ip.ToString();
            log.CreatedDate=DateTime.Now;
            log.MutakabatDurum=value.Durum;
            log.MutakabatTipi=MutakabatTipi.BaBs;
            log.Os=$"{ua.OS.Name} {ua.OS.Version}";
            session.SaveChanges();
            log.RefMutakabat=value.Id;
            session.MutakabatLog.Add(log);
            session.SaveChanges();
        }

        // PUT api/values/5
        [HttpPut ("{id}")]
        public void Put (int id, [FromBody] string value) { }

        // DELETE api/values/5
        [HttpDelete ("{id}")]
        public void Delete (int id) { }
        
    }
}