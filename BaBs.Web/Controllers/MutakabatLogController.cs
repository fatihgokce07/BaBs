using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BaBs.Data;
using BaBs.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using BaBs.Web.ViewModels;
namespace BaBs.Web.Controllers
{
    //[Authorize ("Bearer")]
    [Route ("api/[controller]")]
    public class MutakabatLogController:BaseController
    {
        BaBsContext session;
        public MutakabatLogController(BaBsContext context){
            this.session=context;
        }
        [HttpGet]
        public JsnResponse GetAll (int? page,string logTipi,string model) 
        {     
            
            int baslangic = ((page??1) - 1) * PAGE_COUNT;
            MutakabatLogViewModel log=JsonConvert.DeserializeObject<MutakabatLogViewModel>(model);
            var l = this.session.MutakabatLog;
            JsnResponse jsn=new JsnResponse();
            MutakabatTipi guncelTip = logTipi == "babs" ? MutakabatTipi.BaBs:MutakabatTipi.Cari;
            var qq=(from m in session.MutakabatLog                       
                    where m.MutakabatTipi==guncelTip
                    select m
                    // select new {
                     
                    //     Mutakabat=guncelTip==MutakabatTipi.BaBs ? 
                    //                (from c in session.BaBsMutakabat
                    //                 join b in session.BaBsCaris on c.CariKod  equals b.CARI_KOD
                    //                 where m.RefMutakabat==c.Id 
                    //                 select new 
                    //                 {CariIsim=b.CARI_ISIM,Yil=c.Yil,Ay=c.Ay}).Take(1).FirstOrDefault()
                    //                 :
                    //                 (from c2 in session.CariMutakabat
                    //                 join b2 in session.BaBsCaris on c2.CariKod  equals b2.CARI_KOD
                    //                 where m.RefMutakabat==c2.Id 
                    //                 select new 
                    //                 {CariIsim=b2.CARI_ISIM,Yil=c2.Yil,Ay=c2.Ay}).Take(1).FirstOrDefault(),
                    //                 // new {CariIsim="",Yil=1900,Ay=12},
                    //     Os=m.Os,Browser=m.Browser,MutakabatTipi=m.MutakabatTipi,
                    //     MutakabatDurum=m.MutakabatDurum,CreatedDate=m.CreatedDate,
                    //     Ip=m.Ip

                    // }
                    );
           
            if(guncelTip == MutakabatTipi.BaBs){
                var q2=(from m in qq
                select new {                       
                            Mutakabat=(from c in session.BaBsMutakabat
                            join b in session.BaBsCaris on c.CariKod  equals b.CARI_KOD
                            where m.RefMutakabat==c.Id 
                            select new 
                            {CariIsim=b.CARI_ISIM,Yil=c.Yil,Ay=c.Ay}).Take(1).FirstOrDefault(),
                            Os=m.Os,Browser=m.Browser,MutakabatTipi=m.MutakabatTipi,
                            MutakabatDurum=m.MutakabatDurum,CreatedDate=m.CreatedDate,Ip=m.Ip
                } 
                ); 
                var q=q2.Where(x=>
                (string.IsNullOrEmpty(log.CariIsim) ? true:x.Mutakabat.CariIsim.Contains(log.CariIsim)));
                jsn.ToplamSayi=q.Count(); 
                jsn.Data=q.OrderByDescending(x=>x.CreatedDate).Skip(baslangic).Take(PAGE_COUNT);             
            }else{
                var q2=(from m in qq
                select new {                       
                            Mutakabat=(from c in session.CariMutakabat
                            join b in session.CariMutakabatBilgi on c.CariKod  equals b.CariKod
                            where m.RefMutakabat==c.Id 
                            select new 
                            {CariIsim=b.CariIsim,Yil=c.Yil,Ay=c.Ay}).Take(1).FirstOrDefault(),
                            Os=m.Os,Browser=m.Browser,MutakabatTipi=m.MutakabatTipi,
                            MutakabatDurum=m.MutakabatDurum,CreatedDate=m.CreatedDate,Ip=m.Ip
                } 
                ); 
                var q=q2.Where(x=>
                (string.IsNullOrEmpty(log.CariIsim) ? true:x.Mutakabat.CariIsim.Contains(log.CariIsim)));
                jsn.ToplamSayi=q.Count(); 
                jsn.Data=q.OrderByDescending(x=>x.CreatedDate).Skip(baslangic).Take(PAGE_COUNT);            
            }
           
            return jsn;
            
        }
    }
}