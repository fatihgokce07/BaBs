using BaBs.Web.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;
using BaBs.Data;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
namespace BaBs.Web.Controllers
{
    public class UserModel
    {
        public int id { get; set; }
        public string username { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string token { get; set; }
        public double expireTime { get; set; }
        public string email { get; set; }
    }
    [Route("api/[controller]")]
    public class TokenController : Controller
    {

         BaBsContext session;
         public TokenController(BaBsContext db){
             this.session=db;
         }
        private double MilliTimeStamp(DateTime theDate)
        {
            DateTime d1 = new DateTime(1970, 1, 1);
            DateTime d2 = theDate.ToUniversalTime();
            TimeSpan ts = new TimeSpan(d2.Ticks - d1.Ticks);

            return ts.TotalMilliseconds;
        }
        
        // GET api/<controller>
       
        [AllowAnonymous]
        public IActionResult Get(string username, string password)
        {
            var user= session.Users.SingleOrDefault(x=>x.Email==username && x.Password==password);
            if (user!=null)
            {
                double expireTime = MilliTimeStamp(DateTime.Now.AddMinutes(JwtManager.ExpireMinutes));
                var requestAt = DateTime.Now;
                var expiresIn = requestAt + TokenAuthOption.ExpiresSpan;
             
                var token =JwtManager.GenerateToken(username);
                Setting setting=session.Settings.FirstOrDefault();
                var userAgent = Request.Headers["User-Agent"];
                UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);
                var ip= Request.HttpContext.Connection.RemoteIpAddress;
                UserLog userLog=new UserLog{
                    UserId=user.Id,LoginDate=DateTime.Now,ClientIp=ip.ToString(),
                    Browser=$"{ua.Browser.Name} ${ua.Browser.Version}",
                    Platform=$"{ua.OS.Name} ${ua.OS.Version}"};
                session.UserLogs.Add(userLog);
                session.SaveChanges();
                return Json(new RequestResult
                {
                    State = RequestState.Success,
                    Data = new
                    {
                        requertAt = requestAt,
                        expiresIn = expireTime, //TokenAuthOption.ExpiresSpan.TotalMilliseconds,
                        tokeyType = TokenAuthOption.TokenType,
                        accessToken = token,
                        user=user,
                        setting=setting
                    }
                });

            }
            else
            {
                return Json(new RequestResult
                {
                    State = RequestState.Failed,
                    Msg = "Username or password is invalid"
                });
            }
            //return null;
            //throw new HttpResponseException(HttpStatusCode.Unauthorized);
        }
     
        private bool CheckUser(string username, string password)
        {
            
            return session.Users.Where(x=>x.Email==username && x.Password==password).Count()>0 ;
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}