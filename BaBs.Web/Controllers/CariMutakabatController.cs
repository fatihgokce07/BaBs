using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using BaBs.Data;
using BaBs.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Data;
using System.Data.Common;
using MySql.Data.MySqlClient;
using BaBs.Web.ViewModels;
namespace BaBs.Web.Controllers
{
    [Authorize ("Bearer")]
    [Route ("api/[controller]")]
    public class CariMutakabatController:BaseController
    {
        BaBsContext session;
        public CariMutakabatController(BaBsContext db)
        {
            this.session=db;
        }
        [HttpGet("getAll")]
        public JsnResponse GetAll(int? page,string model)
        {
            int baslangic = ((page??1) - 1) * PAGE_COUNT;
            BaBsMutakabatViewModel mutakabat=JsonConvert.DeserializeObject<BaBsMutakabatViewModel>(model);
            var l = this.session.CariMutakabat;
            JsnResponse jsn=new JsnResponse();
        
            var qq=(from m in session.CariMutakabat
                    join b in session.CariMutakabatBilgi on m.CariKod  equals b.CariKod
                    where m.Yil==b.Yil && m.Ay==b.Ay
                    select new {
                        CariKod=m.CariKod,CariIsim=b.CariIsim,
                        Yil=m.Yil.ToString(),Ay=m.Ay.ToString(),
                        CreatedDate=m.CreatedDate,UpdatedDate=m.UpdatedDate,
                        Durum= m.Durum,Id=m.Id
                    }
                    );          
            var q=qq.Where(x=>
             (string.IsNullOrEmpty(mutakabat.CariKod) ? true:x.CariKod.Contains(mutakabat.CariKod))
             && (mutakabat.Yil==0 ? true: x.Yil.Contains(mutakabat.Yil.ToString()))      
             && (string.IsNullOrEmpty(mutakabat.CariIsim) ? true: x.CariIsim.Contains(mutakabat.CariIsim) )    
             );
            jsn.ToplamSayi=q.Count(); 
            jsn.Data=q.OrderByDescending(x=>x.CreatedDate).Skip(baslangic).Take(PAGE_COUNT);
            return jsn;
        }
        [HttpGet]
        public object Get (string cariKodu) 
        {
            int[]yillar=session.CariMutakabatBilgi.Where(x=>x.CariKod==cariKodu).Select(x=>x.Yil).Distinct().ToArray();
            int[]aylar=session.CariMutakabatBilgi.Where(x=>x.CariKod==cariKodu).Select(x=>x.Ay).Distinct().ToArray();
            return new{Yillar=yillar,Aylar=aylar};
        }
        [HttpGet("getCariHesap")]
        public CariMutakabatBilgi GetCariHesap (string cariKodu,int yil,int ay) 
        {
            return session.CariMutakabatBilgi.FirstOrDefault(x=> x.CariKod==cariKodu && 
                   x.Yil==yil && x.Ay==ay);
        }
        [HttpPost]
        public void Post ([FromBody] CariMutakabat value) { 
           
            if(value.Id>0){
                var mutakabat=session.CariMutakabat.AsNoTracking().SingleOrDefault(x=>x.Id==value.Id);
                session.Entry(value).State = EntityState.Modified;
                value.UpdatedDate=DateTime.Now;              

            } else{
                bool dahaOnceVarmi=session.CariMutakabat
                    .Where(x=>x.Ay==value.Ay && x.Yil== value.Yil && value.CariKod==value.CariKod)
                    .Count()>0;
                if(dahaOnceVarmi){
                    throw new Exception($"Daha önceden yil:{value.Yil} ay:{value.Ay}  için muatakabat yapılmış");
                }
                value.CreatedDate=DateTime.Now;
                session.CariMutakabat.Add(value);
            }
            var userAgent = Request.Headers["User-Agent"];
            UserAgent.UserAgent ua = new UserAgent.UserAgent(userAgent);
            var ip= Request.HttpContext.Connection.RemoteIpAddress;
            MutakabatLog log=new MutakabatLog();
            log.Browser=$"{ua.Browser.Name} ${ua.Browser.Version}";
            log.Ip=ip.ToString();
            log.CreatedDate=DateTime.Now;
            log.MutakabatDurum=value.Durum;
            log.MutakabatTipi=MutakabatTipi.Cari;
            log.Os=$"{ua.OS.Name} {ua.OS.Version}";
            session.SaveChanges();
            log.RefMutakabat=value.Id;
            session.MutakabatLog.Add(log);
            session.SaveChanges();
        }


    }
}