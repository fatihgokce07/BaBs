namespace BaBs.Web.Models
{
    public class JsnResponse
    {
        public bool Basarilimi { get; set; }
        public object Data { get; set; }
        public int ToplamSayi { get; set; }
    }
}