import { Component, OnInit,ViewChild } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {AlertService,SettingService} from '../_services/index';

import { Location } from '@angular/common';
import {Observable} from 'rxjs/Observable';
import {BaseComponent} from '../_shared/index';
@Component({
    moduleId:module.id,
    templateUrl: 'setting.component.html',
    styleUrls: ['setting.component.css'],
    providers:[SettingService]
})

export class SettingComponent extends BaseComponent implements OnInit {
    model:any={};
    formData:FormData;
    @ViewChild("fileInput") fileInput;
    loading:boolean=true;
    currentJustify = 'fill';
        
    constructor(
        private router: Router,
        private activeRoute:ActivatedRoute,
        private alertService: AlertService,
        private location: Location,
        private settingService: SettingService
       
    ) {super();}

    ngOnInit() {       
        this.settingService.get()
        .subscribe(
            data=>{
                this.model=data;
                this.loading=false;
            },
            error=>{
                this.alertService.error(this.getErrorMsg(error));
                this.unautrizedControl(this.router,error);
            }
        )
    }
   
    onSubmit(event){
        
        
        let fileToUpload;
        if(this.fileInput){
            let fi = this.fileInput.nativeElement;
            if (fi.files && fi.files[0]) {
                fileToUpload = fi.files[0];
               
            }
        }
        
        this.settingService.create(this.model,fileToUpload)
            .subscribe(
                data=>{
                    this.alertService.success("Ayarlar kaydedildi");

                },
                error=>{
                    this.alertService.error(error);
                }
            )
    }
   


}