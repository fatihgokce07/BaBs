import { Component, OnInit } from '@angular/core';
import {BaseComponent} from '../_shared/index';
import * as moment from 'moment';
import {BaBsMutakabatService,AlertService,SettingService} from '../_services';
import {User,Setting} from '../_models/index';
import { Router,ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
@Component({
    moduleId: module.id,
    templateUrl: './babs-mutakabat.component.html',
    styleUrls:['babs.css'],
    providers:[BaBsMutakabatService,SettingService]
})

export class BaBsMutakabatComponent extends BaseComponent implements OnInit {
    fortmatDated:string;
    yillar:number[];
    aylar:number[];
    guncelAy:number;
    guncelYil:any;
    guncelUser:User;
    setting?:Setting;
    seciliCari:any;
    loading=true;
    disabled=false;
    babs={mutabik:undefined};
    mutabikButtonsDisabled=true;
    id?:number;
    ayIsimler = [{ ad: 'Ocak', val: 1 }, { ad: 'Şubat', val: 2 }, { ad: 'Mart', val: 3 },
    { ad: 'Nisan', val: 4 }, { ad: 'Mayıs', val: 5 }, { ad: 'Haziran', val: 6 },
    { ad: 'Temmuz', val: 7 }, { ad: 'Ağustos', val: 8 }, { ad: 'Eylül', val: 9 },
    { ad: 'Ekim', val: 10 }, { ad: 'Kasım', val: 11 }, { ad: 'Aralık', val: 12 }];
    constructor(
        private babsCariService:BaBsMutakabatService,
        private alertService: AlertService,
        private settingService:SettingService,
        private router:Router,
        private activeRoute:ActivatedRoute,
        private location: Location
    ) { 
        super();
    }

    ngOnInit() { 
        // this.yillar=[2017,2016,2015];
        // this.aylar=[4,10,9,8,7];
      
        let date=new Date();
        this.fortmatDated=moment(date).format("DD.MM.YYYY");
        this.guncelUser=this.getCurrentUser();
        this.babsCariService.getByCariKod(this.guncelUser.cariKodu)
        .subscribe(
            data=>{
               
                this.aylar=data.aylar;
                this.yillar=data.yillar;
               
                this.loading=false;    
                this.editControl();            
            },
            error=>{
                
                this.alertService.error(error);
                this.loading=false;
                this.unautrizedControl(this.router,error);
            }
        );
        this.settingService.get().subscribe(data=>
            {
                this.setting=data;
            }
        );
    }
    getCari(){
        this.loading=true;
        this.disabled=true;
        if(this.guncelAy && this.guncelYil){
            this.babsCariService.getCari(this.guncelUser.cariKodu,
                this.guncelYil,this.guncelAy).subscribe(
                    data=>{
                        this.seciliCari=data;
                        this.disabled=false;
                        this.loading=false;
                        this.mutabikButtonsDisabled=false;
        
                    },error=>{
                        this.alertService.error(error);
                        this.disabled=false;
                        this.loading=false;
                    }
                );
        }else{
            this.loading=false;
            this.disabled=true;
        }
       
    }
   
    public get getMonthName():string{
        if(this.guncelAy){
           let ay=this.ayIsimler.find(e=>{
               return e.val==this.guncelAy
           });
           return ay.ad;
        }else{
            return "";
        }
    }
    detectmob() { 
        if( navigator.userAgent.match(/Android/i)
        || navigator.userAgent.match(/webOS/i)
        || navigator.userAgent.match(/iPhone/i)
        || navigator.userAgent.match(/iPad/i)
        || navigator.userAgent.match(/iPod/i)
        || navigator.userAgent.match(/BlackBerry/i)
        || navigator.userAgent.match(/Windows Phone/i)
        ){
            //console.log(true);
           return true;
         }
        else {
            
           return false;
         }
       
    }
    onSubmit(durum){
        let user:User=this.getCurrentUser();
        let mutakabat={
            cariKod:user.cariKodu,
            yil:this.guncelYil,
            ay:this.guncelAy,
            refUserId:user.id,
            durum:durum,id:this.id
        };
        this.babsCariService.create(mutakabat)
        .subscribe(
            data=>{
                this.alertService.success("Kaydedildi");
                this.babs.mutabik=durum;
               
            },
            error=>{
                let msg=""
                this.alertService.error(this.getErrorMsg(error));
            }
        );
        let elmeent=document.getElementById('page-header');
        elmeent.scrollIntoView();
        
    }
    goBack(){
        this.location.back();
    }
    private findMaxValue(){
        if(this.yillar && this.yillar.length>0){
            this.guncelYil=this.yillar.reduce((a,b)=>{
                return Math.max(a,b);
            });
        }
        if(this.aylar && this.aylar.length>0){
            this.guncelAy=this.aylar.reduce((a2,b2)=>{
                return Math.max(a2,b2);
            });
        }
       
    }
    private editControl(){
        this.activeRoute.params.subscribe(params=>{
            if(params["id"]){
                this.id=+params["id"];
                this.guncelYil=+params["yil"];
                this.guncelAy=parseInt(params["ay"]);
                this.getCari();
                this.babs.mutabik= params["durum"]=="true" ? true :false;
            }else{
                this.findMaxValue();
            }
        });
    }
}