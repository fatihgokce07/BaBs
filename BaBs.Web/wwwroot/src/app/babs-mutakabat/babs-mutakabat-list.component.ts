import { Component, OnInit } from '@angular/core';
import {AlertService,BaBsMutakabatService} from '../_services/index';
import {Observable} from 'rxjs/Observable';
import { Router} from '@angular/router';
import 'rxjs/add/observable/of';
import {BabsMutakabat} from '../_models/index';
import * as moment from 'moment';
import {BaseComponent} from '../_shared/index';
@Component({
    moduleId: module.id,
    templateUrl: './babs-mutakabat-list.component.html',
    providers:[BaBsMutakabatService]
})
export class BaBsMutakabatListComponent extends BaseComponent implements OnInit {
    model:BabsMutakabat;
    show:Observable<boolean>;
    currentPage: number;
    numPages: number;
    totalItems: number;
    itemsPerPage: number;
    mutakabats:object;//object[];
    loading:boolean=false;
    constructor(
        private router:Router,
        private babsService:BaBsMutakabatService,
        private alertService:AlertService
    ) { 
        super();
    }

    ngOnInit() { 
        this.model=new BabsMutakabat();
        this.itemsPerPage = 2;      
        this.loadAllMutakabats(null);
        this.currentPage = 1;
        
    }
   
    public pageChanged(page){
        console.log("pageChanged: ", page);
        //this.getActorsList();
        this.loadAllMutakabats(page);
    }
    getirData(data){
        if ((data === undefined || data === "") || data.length >= 3) {
            this.loadAllMutakabats(null);

        }

    }
  
    private loadAllMutakabats(page) {
        this.loading=true;
        this.babsService.getAll(this.model,page).subscribe(
            res=>{
              this.mutakabats=res.data as object[];
              this.totalItems=res.toplamSayi;
              this.show= Observable.of(this.totalItems>this.itemsPerPage);
              this.loading=false;
            },
            error=>{
                this.loading=false;
                this.alertService.error(error);
                this.unautrizedControl(this.router,error);
            }
          
        );
    }
    // unautrizedControl(error:Response | any){
    //     if(error.status != undefined){
    //         if(error.status==401){
    //             this.router.navigate(['/login']);
    //         }
    //     }
    // }
}