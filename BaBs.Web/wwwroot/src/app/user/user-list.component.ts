import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AlertService,UserService} from '../_services/index';
import{User} from '../_models/index';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {BaseComponent} from '../_shared/index';
@Component({   
    moduleId:module.id,
    templateUrl: './user-list.component.html',
    //styleUrls: ['./name.component.css']
})
export class UserListComponent extends BaseComponent implements OnInit {
    users:object;
    model:User;
    public currentPage: number;
    public numPages: number;
    public totalItems: number;
    public itemsPerPage: number;
    show:Observable<boolean>;
    loading:boolean=false;
    constructor(
        private router:Router,
        private userService:UserService, 
        private alertService: AlertService) { 
            super();
        }

    ngOnInit() { 
        this.model=new User();
        this.itemsPerPage = 2;
        console.log("ngOnInit: ");
        this.loadAllUsers(null);
        this.currentPage = 1;
        
    }
    private loadAllUsers(page) {
        this.loading=true;
        this.userService.getAll(this.model,page).subscribe(
            res=>{
              this.loading=false;
              this.users=res.data;
              this.totalItems=res.toplamSayi;
              this.show= Observable.of(this.totalItems>this.itemsPerPage);
            },
            error=>{
                this.alertService.error(this.getErrorMsg(error));
                this.unautrizedControl(this.router,error);
            }
          
        );
    }
    public pageChanged(page){
        console.log("pageChanged: ", page);
        //this.getActorsList();
        this.loadAllUsers(page);
    }
    getirData(data){
        if ((data === undefined || data === "") || data.length >= 3) {
            this.loadAllUsers(null);

        }

    }
}