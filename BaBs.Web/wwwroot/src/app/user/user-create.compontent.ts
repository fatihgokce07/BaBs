import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import {AlertService,UserService} from '../_services/index';
import {User} from '../_models/user';
import { Location } from '@angular/common';
import {Observable} from 'rxjs/Observable';
@Component({
    moduleId:module.id,
    templateUrl: './user-create.component.html',
    //styleUrls: ['./name.component.css']
    
})
export class UserCreateComponent implements OnInit {
    loading: boolean = false;
    model: any={};
    submitted = false;
    isEdit?:boolean;
    constructor(
        private router: Router,
        private activeRoute:ActivatedRoute,
        private alertService: AlertService,
        private location: Location,
        private userService: UserService
    ) {}

    ngOnInit() {
        this.isEdit=false;
        this.activeRoute.params.subscribe(params=>{
            if(params["id"]){
                let id:number=+params["id"];
                this.isEdit=true;
                this.userService.getById(id).subscribe
                (data=>{
                    this.model=data;
                })
            }else{
                this.model.isActive=true;
            }
           
        });
    }
    onSubmit() {
        this.submitted = true;
        console.log(JSON.stringify(this.model));
        this.loading = true;
        this.userService.create(this.model)
            .subscribe(
                data => {
                    this.alertService.success('Kullanıcı kaydedildi.', true);
                    this.router.navigate(['/users']);
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
    saveUser() {
        this.loading = true;
    }
    goBack(): void {
        this.location.back();
    }
}