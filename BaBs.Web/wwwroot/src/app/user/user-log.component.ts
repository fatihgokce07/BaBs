import { Component, OnInit } from '@angular/core';
import {AlertService,UserService} from '../_services/index';
import {Observable} from 'rxjs/Observable';
import { Router} from '@angular/router';
import 'rxjs/add/observable/of';
import {BaseComponent} from '../_shared/index';

@Component({
    moduleId:module.id,
    templateUrl: 'user-log.component.html'
})

export class UserLogComponent extends BaseComponent implements OnInit {
    model:any;
    show:Observable<boolean>;
    currentPage: number;
    numPages: number;
    totalItems: number;
    itemsPerPage: number;
    loading:boolean=false;
    userLogs:any;
    constructor(
        private router:Router,
        private userService:UserService,
        private alertService:AlertService
    ) { 
        super();
    }

    ngOnInit() {
        this.model=new Object();
        this.itemsPerPage = this.itemPerPage;      
        this.loadUserLogs(null);
        this.currentPage = 1;
     }
    pageChanged(page){       
        this.loadUserLogs(page);
    }
    loadUserLogs(page){
        this.loading=true;
        this.userService.getUserLog(page).subscribe(
            res=>{
                this.userLogs=res.data;
                this.totalItems=res.toplamSayi;
                this.show=Observable.of(this.totalItems>this.itemPerPage);
                this.loading=false;
            },
            error=>{
                this.alertService.error(this.getErrorMsg(error));
                this.unautrizedControl(this.router,error);
            }
        )
    }
}