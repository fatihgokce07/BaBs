import { Component, OnInit,ViewChild,ElementRef } from '@angular/core';
import {BaseComponent} from '../_shared/index';
import * as moment from 'moment';
import {CariMutakabatService,AlertService,SettingService} from '../_services';
import {User,Setting} from '../_models/index';
import { Router,ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';
import jsPDF from 'jspdf';
import * as html2canvas from "html2canvas";
@Component({
    moduleId: module.id,
    templateUrl: './cari-mutakabat.component.html',
    styleUrls: ['./cari-mutakabat.component.css'],
    providers:[CariMutakabatService,SettingService]
})
export class CariMutakabatComponent extends BaseComponent implements OnInit {
    fortmatDated:string;
    yillar:number[];
    aylar:number[];
    guncelAy:number;
    guncelYil:any;
    guncelUser:User;
    setting?:Setting;
    seciliCari:any;
    loading=true;
    disabled=false;
    babs={mutabik:undefined};
    mutabikButtonsDisabled=true;
    id?:number;
    @ViewChild('test') el: ElementRef;
    constructor(
        private cariMutakabatService:CariMutakabatService,
        private alertService: AlertService,
        private settingService:SettingService,
        private router:Router,
        private activeRoute:ActivatedRoute,
        private location: Location
    ) { 
        super();
    }

    ngOnInit() { 
        this.yillar=[2017,2016,2015];
        this.aylar=[1,2,3,4,5,6,7,8,9];
        let date=new Date();
        this.fortmatDated=moment(date).format("DD.MM.YYYY");
        this.guncelUser=this.getCurrentUser();
    
        this.cariMutakabatService.getByCariKod(this.guncelUser.cariKodu)
        .subscribe(
            data=>{
               
                this.aylar=data.aylar;
                this.yillar=data.yillar;
               
                this.loading=false;    
                this.editControl();            
            },
            error=>{
                
                this.alertService.error(error);
                this.loading=false;
                this.unautrizedControl(this.router,error);
            }
        );
        this.settingService.get().subscribe(data=>
            {
                this.setting=data;
            }
        );
    }
    getCari(){
        this.loading=true;
        this.disabled=true;
        if(this.guncelAy && this.guncelYil){
            this.cariMutakabatService.getCariHesap(this.guncelUser.cariKodu,
                this.guncelYil,this.guncelAy).subscribe(
                    data=>{
                        this.seciliCari=data;
                        this.disabled=false;
                        this.loading=false;
                        this.mutabikButtonsDisabled=false;
        
                    },error=>{
                        this.alertService.error(error);
                        this.disabled=false;
                        this.loading=false;
                    }
                );
        }else{
            this.loading=false;
            this.disabled=true;
        }
       
    }
    onSubmit(durum){
        let user:User=this.getCurrentUser();
        let mutakabat={
            cariKod:user.cariKodu,
            yil:this.guncelYil,
            ay:this.guncelAy,
            refUserId:user.id,
            durum:durum,id:this.id
        };
        this.cariMutakabatService.create(mutakabat)
        .subscribe(
            data=>{
                this.alertService.success("Kaydedildi");
                this.babs.mutabik=durum;
               
            },
            error=>{
                let msg=""
                this.alertService.error(this.getErrorMsg(error));
            }
        );
        let elmeent=document.getElementById('page-header');
        elmeent.scrollIntoView();
        
    }
    goBack(){
        this.location.back();
    }
    toPdf(){      
        
        let pdf = new jsPDF('p', 'mm');//jsPDF('l', 'pt', 'a4');
        let options = {
            pagesplit: true
        };
        
        html2canvas(this.el.nativeElement).then(canvas => {
            var imgData = canvas.toDataURL("image/png",1.0);            
            //pdf.addImage(imgData,10,10,canvas.width,canvas.height);
            pdf.addImage(imgData,10,10,180,150);
            console.log(pdf.addImage);
            pdf.save("test.pdf");
           
        });
       
    }
    private findMaxValue(){
        if(this.yillar && this.yillar.length>0){
            this.guncelYil=this.yillar.reduce((a,b)=>{
                return Math.max(a,b);
            });
        }
        if(this.aylar && this.aylar.length>0){
            this.guncelAy=this.aylar.reduce((a2,b2)=>{
                return Math.max(a2,b2);
            });
        }
       
    }
    private editControl(){
        this.activeRoute.params.subscribe(params=>{
            if(params["id"]){
                this.id=+params["id"];
                this.guncelYil=+params["yil"];
                this.guncelAy=parseInt(params["ay"]);
                this.getCari();
                this.babs.mutabik= params["durum"]=="true" ? true :false;
            }else{
                this.findMaxValue();
            }
        });
    }
}