import { Component, OnInit } from '@angular/core';
import {AlertService,CariMutakabatService} from '../_services/index';
import {Observable} from 'rxjs/Observable';
import { Router} from '@angular/router';
import 'rxjs/add/observable/of';
import {BabsMutakabat} from '../_models/index';
import * as moment from 'moment';
import {BaseComponent} from '../_shared/index';
@Component({
    moduleId: module.id,
    templateUrl: 'cari-mutakabat-list.component.html',
    providers:[CariMutakabatService]
})

export class CariMutakabatListComponent extends BaseComponent implements OnInit {
    model:BabsMutakabat;
    show:Observable<boolean>;
    currentPage: number;
    numPages: number;
    totalItems: number;
    itemsPerPage: number;
    mutakabats:object;//object[];
    loading:boolean=false;
    constructor(
        private router:Router,
        private cariService:CariMutakabatService,
        private alertService:AlertService
    ) { 
        super();
    }
    ngOnInit() { 
        this.model=new BabsMutakabat();
        this.itemsPerPage = this.itemPerPage;      
        this.loadAllMutakabats(null);
        this.currentPage = 1;
    }
    public pageChanged(page){       
        this.loadAllMutakabats(page);
    }
    getirData(data){
        if ((data === undefined || data === "") || data.length >= 3) {
            this.loadAllMutakabats(null);

        }

    }
    // getDateFormated(date){
    //     if(date){
    //         return moment(date).format("DD.MM.YYYY hh:mm");   
    //     }else{
    //         return "";
    //     }
    // }
    private loadAllMutakabats(page) {
        this.loading=true;
        this.cariService.getAll(this.model,page).subscribe(
            res=>{
              this.mutakabats=res.data as object[];
              this.totalItems=res.toplamSayi;
              this.show= Observable.of(this.totalItems>this.itemsPerPage);
              this.loading=false;
            },
            error=>{
                this.loading=false;
                this.alertService.error(error);
                this.unautrizedControl(this.router,error);
            }
          
        );
    }
}