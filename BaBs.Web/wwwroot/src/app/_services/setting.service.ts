import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response,URLSearchParams } from '@angular/http';

import {JsnResponse} from '../_models/jsn-response';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {BaseService} from './base.service';
@Injectable()
export class SettingService extends BaseService{

    constructor(private http:Http) {
        super();
     }
     get():Observable<any>{
         let ro=super.getRequestOpt();
         return this.http.get('/api/settings/',ro)
                .map(this.extractData).catch(this.handleError);
     }
     create(setting: any,fileToUpload: any) {
        let input = new FormData();
        input.append("file", fileToUpload);
        input.append("setting",JSON.stringify(setting));
        //let ro=this.getRequestOpt();
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data.accessToken });
   
        let ro = new RequestOptions({headers:headers});
        //let headers = new Headers();
       
        let view={setting:setting,file:input};
        return this.http.post('/api/settings',input, ro)
        .map(this.extractData)
        .catch(this.handleError);
    }
}