import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {BaseService} from './base.service';
import {JsnResponse} from '../_models/jsn-response';
@Injectable()
export class MutakabatLogService extends BaseService {
    constructor(private http:Http){
        super();
    }
    getAll(model:object,logTipi:string,page?:number):Observable<JsnResponse> {
        let ro=super.getRequestOpt();
        let params: URLSearchParams = new URLSearchParams();
        params.set('page', page==null ? null : page.toString()); 
        params.set('logTipi',logTipi);
        params.set('model',JSON.stringify(model));
      
        ro.params=params; 
        //ro.body=JSON.stringify( curentUser);
        //ro.params=params; 
        return this.http.get('/api/mutakabatlog/',ro ).map(this.extractData)
        .catch(this.handleError);
    }
}