import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {BaseService} from './base.service';
import {JsnResponse} from '../_models/jsn-response';

@Injectable()
export class CariMutakabatService extends BaseService {

   constructor(private http: Http) { 
        super();
    }
    getAll(model:object,page?:number):Observable<JsnResponse> {        
        let ro=super.getRequestOpt();
        let params: URLSearchParams = new URLSearchParams();
        params.set('page', page==null ? null : page.toString()); 
        params.set('model',JSON.stringify(model));
      
        ro.params=params; 
        //ro.body=JSON.stringify( curentUser);
        //ro.params=params; 
        return this.http.get('/api/carimutakabat/getAll',ro ).map(this.extractData)
        .catch(this.handleError);
    }
    getByCariKod(cariKodu:string):Observable<any> {
        let ro=super.getRequestOpt();
        let params: URLSearchParams = new URLSearchParams();
        params.set('cariKodu',cariKodu);       
      
        ro.search=params; 
       
        return this.http.get('/api/carimutakabat/',ro ).map(this.extractData)
        .catch(this.handleError);
    }
    getCariHesap(cariKodu:string,yil:number,ay:number):Observable<any> {
        let ro=super.getRequestOpt();
        let params: URLSearchParams = new URLSearchParams();
        params.set('cariKodu',cariKodu);       
        params.set('yil',yil.toString()); 
        params.set('ay',ay.toString());   
        ro.search=params; 
       
        return this.http.get('/api/carimutakabat/getCariHesap',ro ).map((res:Response) => res.json());
    }
    create(model: any) {     
        let ro=this.getRequestOpt();

        return this.http.post('/api/carimutakabat', model, ro)
        .map(this.extractData)
        .catch(this.handleError);
    }
}