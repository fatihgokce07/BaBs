import { Http, Headers, RequestOptions, Response,URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Injectable ,ReflectiveInjector,Injector} from '@angular/core';
import { Router} from '@angular/router';
@Injectable()
export class BaseService{
    private router:Router;
    constructor() { 
        // * var providers = ReflectiveInjector.resolve([Car, Engine]);
        // * var injector = ReflectiveInjector.fromResolvedProviders(providers);
        // * expect(injector.get(Car) instanceof Car).toBe(true);

        // var providers = ReflectiveInjector.resolve([Router]);
        // var injector=ReflectiveInjector.fromResolvedProviders(providers);
        // this.router=injector.get(Router) as Router;
    
    }

    getRequestOpt():RequestOptions{
        let currentUser = JSON.parse(localStorage.getItem('currentUser'));
        let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.data.accessToken,'Content-Type': 'application/json' });
        let ro = new RequestOptions({headers:headers});
        
        return ro;
    }
    extractData(res: Response) {
      console.log(res);
      let body = res.text() ? res.json():{};
      return body;
    }

    handleError(error: Response | any) {
      // In a real world app, we might use a remote logging infrastructure
    //   let errMsg: string;
    //   if (error instanceof Response) {
    //       const body = error.json() || '';
    //       const err = body.error || JSON.stringify(body);
    //       errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    //   } else {
    //       errMsg = error.message ? error.message : error.toString();
    //   }
    //   console.error(errMsg);
    // this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
    //this.router.navigate(['/login']);
      return Observable.throw(error);
    }

}