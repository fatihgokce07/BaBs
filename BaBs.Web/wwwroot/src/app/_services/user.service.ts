import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response,URLSearchParams } from '@angular/http';

import { User } from '../_models/index';
import {JsnResponse} from '../_models/jsn-response';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import {BaseService} from './base.service';
@Injectable()
export class UserService extends BaseService  {
    constructor(private http: Http) { 
        super();
    }
    // private getRequestOpt():RequestOptions{
    //     let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //     let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token,'Content-Type': 'application/json' });
      
    //     let ro = new RequestOptions({headers:headers});
        
    //     return ro;
    // }
    getAll(curentUser:object,page?:number):Observable<JsnResponse> {
        let ro=super.getRequestOpt();
        let params: URLSearchParams = new URLSearchParams();
        params.set('page', page==null ? null : page.toString()); 
       params.set('currentUser',JSON.stringify(curentUser));
      
        ro.params=params; 
        //ro.body=JSON.stringify( curentUser);
        //ro.params=params; 
        return this.http.get('/api/user/',ro ).map(this.extractData)
        .catch(this.handleError);
    }
    getUserLog(page?:number){
        let ro=super.getRequestOpt();
        let params: URLSearchParams = new URLSearchParams();
        params.set('page', page==null ? null : page.toString());       
      
        ro.params=params; 
       
        return this.http.get('/api/user/getUserLog',ro ).map(this.extractData)
        .catch(this.handleError);
    }
    controlExistUser(columnName:string,value:string,tableName:string,id?:number):Observable<boolean>{
      
        let params: URLSearchParams = new URLSearchParams();
        params.set('columnName', columnName);    
        params.set('value', value);  
        params.set('tableName',tableName);
        params.set('id',id==null ? null : id.toString());
        let ro=super.getRequestOpt();
        //requestOptions.search = params;
        ro.search=params;
        //ro.headers=headers;
        console.log(ro.headers);
        return this.http.get('/api/user/ExistUser',ro ).map((res:Response) => res.json());
    }
    getById(id: number) {
        return this.http.get('/api/user/' + id, super.getRequestOpt())
        .map(this.extractData).catch(this.handleError);
    }

    create(user: User) {
        // let params: URLSearchParams = new URLSearchParams();
        // params.set('value', user.username); 
        let ro=this.getRequestOpt();
        //ro.params=params;
        return this.http.post('/api/user', user, ro)
        .map(this.extractData)
        .catch(this.handleError);
    }

    update(user: User) {
        return this.http.put('/api/users/' + user.id, user, this.jwt()).map((response: Response) => response.json());
    }

    delete(id: number) {
        return this.http.delete('/api/users/' + id, this.jwt()).map((response: Response) => response.json());
    }
    
    // private helper methods
    private jwt() {
        // create authorization header with jwt token
        let currentUser = JSON.parse(sessionStorage.getItem('currentUser'));
        if (currentUser && currentUser.token) {
            let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token });
            return new RequestOptions({ headers: headers });
        }
    }
}