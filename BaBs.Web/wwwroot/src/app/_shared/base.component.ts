import { Component } from '@angular/core';
import {AuthGuard} from '../_guards/index';
import {User,Setting} from '../_models';
import {Router} from '@angular/router';
import * as moment from 'moment';
// @Component({  
//   //changeDetection: ChangeDetectionStrategy.OnPush 
// })
export class BaseComponent {
    isAuthrized:boolean;
    readonly itemPerPage=2;
    readonly ckEditorConfig: {} = {
        "basicEntities":false,
        // "uiColor": "#242424",
        "autoParagraph":false,
        "height":"120",
        "toolbarGroups": [ 
        // {"name":"basicstyles","groups":["basicstyles"]},
        {"name":"links","groups":["links"]},
        {"name":"paragraph","groups":["list","blocks"]},
        {"name":"document","groups":["mode"]},
        // {"name":"insert","groups":["insert"]},
        // {"name":"styles","groups":["styles"]},
        {"name":"about","groups":["about"]}
        ],
        "removeButtons": "Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar"
    };
    constructor() {     
    
    }
    public get isAdmin():boolean{
      
        let currentUser:User=this.getCurrentUser();
        if(currentUser){
            return currentUser.isAdmin;
        }else{
            return false;
        }
    }
  
    public get isAuthenticated():boolean{
        //console.log("neden");
        if (localStorage.getItem('currentUser')) {
            var currentUser= JSON.parse(localStorage.getItem('currentUser'));
            let expireTime=new Date(currentUser.data.expiresIn).getTime();
            let currentTime=new Date().getTime();
            if(expireTime>currentTime){
                
                return true;
            }else{
                
                return false;
            }
        }else{
            return false;
        }
        //return AuthGuard.isAuthenticated; //this.isAuthrized;//AuthenticationService.userLoggedIn;
    }
    public getCurrentUser():User{
        if (localStorage.getItem('currentUser')) {
            var currentUser= JSON.parse(localStorage.getItem('currentUser'));
            let user:User=currentUser.data.user;
            return user;
        }else{
            return null;
        }
    }
    public getSetting():Setting{
        if (localStorage.getItem('currentUser')) {
            var currentUser= JSON.parse(localStorage.getItem('currentUser'));
            let set:Setting=currentUser.data.setting;
            return set;
        }else{
            return null;
        }
    }
    getErrorMsg(error: Response | any) {
      
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = error.body || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error._body ? JSON.parse(error._body).Msg : error.toString();
        }
        return errMsg;      
    
      }
    unautrizedControl(router:Router,error:Response | any){
        if(error.status != undefined){
            if(error.status==401){
                router.navigate(['/login']);
            }
        }
    }
    getDateFormated(date){
        if(date){
            return moment(date).format("DD.MM.YYYY HH:mm");   
        }else{
            return "";
        }
    }
 
}