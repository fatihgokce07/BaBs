import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { RemoteExistValidateDirective,HighlightDirective } from '../_directives/index';
import {LoaderComponent} from './index';
import { CKEditorModule } from 'ng2-ckeditor';
@NgModule({
    declarations: [RemoteExistValidateDirective,HighlightDirective,LoaderComponent],
    imports: [ NgbModule,CommonModule,CKEditorModule],
    exports: [ LoaderComponent,RemoteExistValidateDirective,HighlightDirective,NgbModule,
    CKEditorModule],
    providers: [],
    bootstrap: []
})
export class SharedModule {}