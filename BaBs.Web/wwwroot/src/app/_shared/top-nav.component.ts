import { Component, OnInit,Input,ViewChild,ElementRef } from '@angular/core';
import {Router }from '@angular/router';
import {AuthenticationService}from'../_services/authentication.service';
import {BaseComponent} from '../_shared/index';
import {User} from '../_models/index';
@Component({   
    moduleId: module.id,
    selector: 'top-nav',
    templateUrl: './top-nav.component.html',
    styleUrls: ['top-nav.component.css']
})
export class TopNavComponent extends BaseComponent implements OnInit {
    isAuthrized:boolean=true; 
    //userIsAdmin:boolean;
    menuClose:boolean=true;
    @ViewChild('navbarSupportedContent') el: ElementRef;
    constructor(private router: Router,private auth:AuthenticationService ) {
      super();
      //this.userIsAdmin=this.isAdmin();
    }
    ngOnInit() { 
        console.log("top nav ngoninit");       
    }
    get user():User{
        return this.getCurrentUser();
    }
    toggleMenu(){        
        //this.el.nativeElement.classList.toggle('collapse');       
        this.menuClose=!this.menuClose;
    }
    
}