export class Setting{
id:number;
companyName:string;
adress:string;
city:string;
town:string;
country:string;
phone:string;
mobilPhone:string;
email:string;
taxOffice:string;
taxNumber:string;
fax:string;
sicilNo:string;
odaNo:string;
mersisNo:string;
logoPath?:string;
}
// `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
// `CompanyName` varchar(100) DEFAULT NULL,
// `Adress` varchar(250) DEFAULT NULL,
// `City` varchar(100) DEFAULT NULL,
// `Town` varchar(100) DEFAULT NULL,
// `Country` varchar(100) DEFAULT NULL,
// `Phone` varchar(20) DEFAULT '',
// `MobilPhone` varchar(20) DEFAULT NULL,
// `Email` varchar(100) DEFAULT NULL,
// `TaxOffice` varchar(100) DEFAULT NULL,
// `TaxNumber` varchar(50) DEFAULT NULL,
// `Fax` varchar(20) DEFAULT NULL,
// `SicilNo` varchar(50) DEFAULT NULL,
// `OdaNo` varchar(50) DEFAULT NULL,
// `MersisNo` varchar(50) DEFAULT NULL,
// `LogoPath` varchar(200) DEFAULT NULL,