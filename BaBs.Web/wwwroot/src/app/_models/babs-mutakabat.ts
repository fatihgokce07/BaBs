export class BabsMutakabat{
   id:number ;
   cariKod:string;
   yil:number;
   ay:number;
   refUserId?:number;
   createdDate?:string;
   updatedDate?:string;
   durum:boolean;
   cariIsim?:string;
}

// `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
// `CariKod` varchar(50) NOT NULL DEFAULT '',
// `Yil` int(11) NOT NULL,
// `Ay` int(11) NOT NULL,
// `RefUserId` int(11) NOT NULL,
// `CreatedDate` datetime DEFAULT NULL,
// `UpdatedDate` int(11) DEFAULT NULL,
// `Durum` tinyint(1) NOT NULL,