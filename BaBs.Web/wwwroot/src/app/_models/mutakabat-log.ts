export class MutakabatLog{
    id:number;
    os:string;
    browser:string;
    mutakabatTipi:number;
    refMutakabat:number;
}


// `Id` int(11) unsigned NOT NULL AUTO_INCREMENT,
// `Os` varchar(200) DEFAULT NULL,
// `Browser` varchar(200) DEFAULT NULL,
// `MutakabatTipi` int(11) DEFAULT NULL,
// `RefMutakabat` int(11) DEFAULT NULL,
// `MutakabatDurum` tinyint(4) DEFAULT NULL,
// `CreatedDate` datetime DEFAULT NULL,
// `Ip` varchar(40) DEFAULT NULL,