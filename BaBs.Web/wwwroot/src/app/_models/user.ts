export class User {
    id: string;
    userName:string;
    email: string;
    nameSurname: string;
    mobilPhone:string;
    password:string;
    createdDate?:string;
    updatedDate?:string;
    isAdmin:boolean;
    isActive:boolean;
    cariKodu?:string;
    vergiNumarasi?:string;

}
// id: 1,
// userName: "fg",
// email: "fg@gmail.com",
// nameSurname: "fatih",
// mobilPhone: null,
// password: null,
// createdDate: "2017-09-20T17:00:00",
// updatedDate: null,
// isAdmin: true
// public virtual string CariKodu { get; set; }
// public string VergiNumarasi{get;set;}