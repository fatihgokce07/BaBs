import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute} from '@angular/router';
import {AlertService,MutakabatLogService} from '../_services/index';
import {Observable} from 'rxjs/Observable';
import {BaseComponent} from '../_shared/index';
import * as moment from 'moment';
@Component({ 
    moduleId:module.id,
    templateUrl: './mutakabat-log.component.html',
    providers:[MutakabatLogService] 
})
export class MutakabatLogComponent extends BaseComponent implements OnInit {
    model:any;
    show:Observable<boolean>;
    currentPage: number;
    numPages: number;
    totalItems: number;
    itemsPerPage: number;
    logs:object;//object[];
    loading:boolean=false;
    mutakabatTipleri={1:'BaBs',2:'Cari'};
    logTipi:string;
    constructor(
        private alertService:AlertService,
        private logService:MutakabatLogService,
        private router:Router,
        private activeRoute:ActivatedRoute
    ) { 
        super();
    }

    ngOnInit() {
       
        this.activeRoute.params.subscribe(params=>{
            console.log(params["tip"]);
            this.logTipi=params["tip"];
            this.model=new Object();
            this.itemsPerPage=this.itemPerPage;
            this.currentPage=1;
            this.loadLogs(null);
        });
    }
    loadLogs(page){
        this.loading=true;
        this.logService.getAll(this.model,this.logTipi,page).subscribe(
            res=>{               
                this.logs=res.data;
                this.totalItems=res.toplamSayi;
                this.show=Observable.of(this.totalItems > this.itemsPerPage);
                this.loading=false;
            },
            error=>{
                this.loading=false;
                this.alertService.error(error);
                this.unautrizedControl(this.router,error);
            }
        )
    }
    pageChanged(page){
        console.log("pageChanged: ", page);
        //this.getActorsList();
        this.loadLogs(page);
    }
    getDateFormated(date){
        if(date){
            return moment(date).format("DD.MM.YYYY hh:mm");   
        }else{
            return "";
        }
    }
    getirData(data){
        if ((data === undefined || data === "") || data.length >= 3) {
            this.loadLogs(null);

        }

    }

}