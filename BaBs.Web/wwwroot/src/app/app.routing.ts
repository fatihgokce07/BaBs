import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_guards/index';
import { RegisterComponent } from './register/register.component';
import {UserListComponent} from './user/user-list.component';
import {ProjectCreateComponent} from './project/index';
import {SettingComponent} from './setting/setting.component';
import {BaBsMutakabatComponent,BaBsMutakabatListComponent} from './babs-mutakabat/index';
import {MutakabatLogComponent} from './_log/index';
import {CariMutakabatComponent,CariMutakabatListComponent} from './cari-mutakabat/index';
const appRoutes: Routes = [
    { path: '', component: BaBsMutakabatListComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent },  
    //{ path: 'users', component: UserListComponent,canActivate: [AuthGuard] },
    {path:'setting',component:SettingComponent,canActivate:[AuthGuard], data: { title: 'Setting project' }},
    {path:'babs-mutakabat',component:BaBsMutakabatComponent,canActivate:[AuthGuard]},
    {path:'babs-mutakabat-list',component:BaBsMutakabatListComponent,canActivate:[AuthGuard]},
    {path:'mutakabat-log',component:MutakabatLogComponent,canActivate:[AuthGuard]},
    {path:'cari-mutakabat',component:CariMutakabatComponent,canActivate:[AuthGuard]},
    {path:'cari-mutakabat-list',component:CariMutakabatListComponent,canActivate:[AuthGuard]},
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);