using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;
using System.Data;
using System.Data.Common;
using System.Reflection;
namespace BaBs.Data{
    public static class DataReaderExtension{
        public static List<T> MapToList<T>(this DbDataReader dr) where T: new()
        {
            var entity=typeof(T);
            var list=new List<T>();
            var propDict=new Dictionary<string,PropertyInfo>();
            var props=entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            propDict=props.ToDictionary(p=>p.Name.ToUpper(),p=>p);
            while(dr.Read()){
                T newObj=new T();
                for(int index=0;index<dr.FieldCount;index++){
                    if(propDict.ContainsKey(dr.GetName(index).ToUpper()))
                    {
                        var prop=propDict[dr.GetName(index)];
                        var info=propDict[dr.GetName(index).ToUpper()];
                        if((info!=null) && info.CanWrite){
                            var val=dr.GetValue(index);
                            var valType = Convert.ChangeType(val, prop.PropertyType);
                            //info.SetValue(newObj,(val==DBNull.Value)? null:val,null);
                            info.SetValue(newObj,valType);
                        }
                    }
                }
                list.Add(newObj);
            }
            return list;
        }
    }
}