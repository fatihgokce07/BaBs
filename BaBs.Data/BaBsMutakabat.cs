using System;
namespace BaBs.Data
{
    public enum MutakabatDurum{
        MutabikDegil=0, Mutabik=1
    }
    public enum MutakabatTipi{
        BaBs=1,Cari=2
    }
    public class BaBsMutakabat{
        public int Id  { get; set; }
        public string CariKod { get; set; }
        public int Yil { get; set; }
        public int Ay { get; set; }
        public int RefUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Durum { get; set; }
        
    }
    public class CariMutakabat{
        public int Id  { get; set; }
        public string CariKod { get; set; }
        public int Yil { get; set; }
        public int Ay { get; set; }
        public int RefUserId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public bool Durum { get; set; }
        
    }
    public class MutakabatLog{
        public int Id { get; set; }
        public string Os { get; set; }
        public string Browser { get; set; }
        public MutakabatTipi MutakabatTipi  { get; set; }
        public int RefMutakabat { get; set; }
        public  bool MutakabatDurum  { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Ip { get; set; }

    }
    

}