namespace BaBs.Data
{
    public class CariMutakabatBilgi
    {
        public int Id { get; set; }
        public string CariKod { get; set; }
        public string CariIsim { get; set; }
        public decimal Tutar { get; set; }
        public int Yil { get; set; }
        public int Ay { get; set; }
    }
}