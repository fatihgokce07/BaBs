﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Microsoft.EntityFrameworkCore.Infrastructure.Internal;

namespace BaBs.Data
{
    public class BaBsContext:DbContext
    {
        private string _connectionString;
        public BaBsContext(string connectionString) : this()
        {
            connectionString="Server=localhost;database=Deneme1;uid=root;pwd=MyNewPass;";
            _connectionString = connectionString;
        }
        public BaBsContext(DbContextOptions<BaBsContext> options) : base(options)
        {
            var extension = options.FindExtension<MySqlOptionsExtension>();
            _connectionString=extension.ConnectionString;
            //options.Extensions.ToList()[1]
            //_connectionString
        }
        public BaBsContext():base()
        {

        }
    
         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder){
               //optionsBuilder.UseMySql(_connectionString);
                //.UseMySql(@"Server=localhost;database=Deneme1;uid=root;pwd=MyNewPass;");
         }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");
                entity.HasKey(x => x.Id);
                //entity.Property(x => x);
                //entity.HasOne<Role>(x => x.Role);
            });
            modelBuilder.Entity<Setting>(entity =>{
                entity.ToTable("Settings");
                entity.HasKey(x=>x.Id);
            });
            modelBuilder.Entity<BaBsCari>(entity =>{
                entity.ToTable("BABS");
                entity.HasKey(x=>x.ID);
            });
            modelBuilder.Entity<BaBsMutakabat>(enetity =>{
                enetity.ToTable("BaBsMutakabat");
                enetity.HasKey(x=>x.Id);
            });
            modelBuilder.Entity<MutakabatLog>(enetity =>{
                enetity.ToTable("MutakabatLog");
                enetity.HasKey(x=>x.Id);
            });
             modelBuilder.Entity<CariMutakabatBilgi>(enetity =>{
                enetity.ToTable("CariMutakabatBilgi");
                enetity.HasKey(x=>x.Id);
            });
            modelBuilder.Entity<CariMutakabat>(enetity =>{
                enetity.ToTable("CariMutakabat");
                enetity.HasKey(x=>x.Id);
            });
            modelBuilder.Entity<UserLog>(enetity =>{
                enetity.ToTable("UserLog");
                enetity.HasKey(x=>x.Id);
            });
         
        }
         public DbSet<User> Users { get; set; }
         public DbSet<Setting> Settings { get; set; }
         public DbSet<BaBsCari> BaBsCaris{get;set;}
         public DbSet<BaBsMutakabat> BaBsMutakabat{get;set;}
         public DbSet<MutakabatLog> MutakabatLog{get;set;}
         public DbSet<CariMutakabatBilgi> CariMutakabatBilgi{get;set;}
         public DbSet<CariMutakabat> CariMutakabat{get;set;}
         public DbSet<UserLog> UserLogs{get;set;}
    }
    public class User{
        public virtual int Id{get;set;}
        public virtual string UserName{get;set;}
        public virtual string Email{get;set;}
        public virtual string NameSurname{get;set;}
        public virtual string MobilPhone{get;set;}
        public virtual string Password{get;set;}
        public virtual DateTime CreatedDate{get;set;}
        public virtual DateTime? UpdatedDate{get;set;}
        public virtual bool IsAdmin{get;set;}
        public virtual bool IsActive{get;set;}
        public virtual string CariKodu { get; set; }
        public string VergiNumarasi{get;set;} 
    }

    public class Setting{
        public virtual int Id{get;set;}
        public virtual string  CompanyName { get; set; }
        public virtual string Adress { get; set; }
        public virtual string  City { get; set; }   
        public virtual string Town { get; set; }
        public virtual string Country { get; set; }
        public virtual string Phone { get; set; }
        public virtual string MobilPhone { get; set; }
        public virtual string Email { get; set; }
        public virtual string TaxOffice { get; set; }
        public virtual string TaxNumber { get; set; }
        public virtual string Fax { get; set; }
        public virtual string SicilNo { get; set; }
        public virtual string OdaNo { get; set; }
        public virtual string MersisNo { get; set; }
        public virtual string LogoPath { get; set; }
        public string CariKonu { get; set; }
        public string BabsKonu { get; set; }
        public string BabsAciklama { get; set; }

         
    }
    public class BaBsCari{
        public int ID { get; set; } 
        public int YIL { get; set; }
        public int AY  { get; set; }
        public int ISLETME_KODU { get; set; }
        public string FTIRSIP { get; set; }
        public string CARI_ISIM { get; set; }   
        public string ULKE_KODU { get; set; }
        public string BEYANULKEKODU { get; set; }
        public string VERGI_NUMARASI { get; set; }
        public int SAYI { get; set; }
        public double TUTAR { get; set; }
        public string TCKIMLIKNO { get; set; }
        public string CARI_KOD { get; set; }
        public string CARI_ADRES { get; set; }
        public string CARI_IL { get; set; }
        public string CARI_ILCE { get; set; }
        public string CARI_TEL { get; set; }
        public string FAX { get; set; }
        public string EMAIL { get; set; }
        public string VERGI_DAIRESI { get; set; }
    }
    public class CCariBaBs{
        public string CARI_ISIM { get; set; }
        public decimal BS_SAYI { get; set; }
        public decimal BS_TUTAR { get; set; }
        public decimal BA_SAYI { get; set; }
        public decimal BA_TUTAR { get; set; }
    }
    public class UserLog{
        public int Id { get; set; }
        public int UserId { get; set; }
        public DateTime LoginDate { get; set; }
        public string ClientIp { get; set; }
        public string Browser { get; set; }
        public string Platform { get; set; }
        public User User {get;set;}
    }
    
}
